# coding: utf-8
#This is the main file. Run this, to run the program
import random
import pygame
from pygame.locals import *
import math
import sys

import Map
import Soldier

white = (255,255,255)
black = (0,	0,	0)

#Setup
pygame.init()
pygame.font.init()
basicFont = pygame.font.SysFont(None, 48)

FPS = 60 # 60 frames per second
clock = pygame.time.Clock()

done = False
font = pygame.font.SysFont("Calibri", 15)

#window
WIDTH =  1000
HEIGTH = 600
global MainWindow
MainWindow = pygame.display.set_mode((WIDTH, HEIGTH))
level = Map.world(WIDTH, HEIGTH,1)
MainWindow.fill(white)
MainWindow.blit(level.sstr,(0,0))
#MainWindow.blit(level.scol,(0,0))
#MainWindow.blit(level.sobj,(0,0))
pygame.display.flip()
bullets = []
units = []
cspots = []
units.append(Soldier.Soldier(1,(255,50,50),(0,0)))
units.append(Soldier.Soldier(-1,(50,50,255),(980,0)))
while not done:
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT: # Closed from X
			done = True # Stop the Loop
			

	MainWindow.blit(level.sstr,(0,0))
	cspots = []
	for unit in units:
		unit.act()
	for b in bullets:
		b.act()
	MainWindow.blit(level.sobj,(0,0))
	#MainWindow.blit(level.scol,(0,0))
	pygame.display.flip()
	clock.tick(FPS)
	pygame.display.set_caption("FPS: %i" % clock.get_fps())
pygame.quit()
