import pygame
import random
import math
import Polygon

import Vector

def bcol(list,plane):
	for point in list:
		try:
			if not plane.get_at(point) == (255,255,255):
				return True
		except IndexError:
			return True
	return False
		
		
class Bullet(object):
	def __init__(self,pos,vel,dra,mas,dir,shooter):
		self.pos = list(pos)
		self.vel = vel			#velocity	(pixels/turn)
		self.dra = dra			#drag		(slowdow/turn)
		self.mas = mas			#mass		(mass)
		self.dir = list(dir)	#direction 	(unitvector)
		self.shooter = shooter
		#self.colour = (200,200,20)
		self.colour = (random.randint(105,255),random.randint(105,255),random.randint(105,255))
		self.pos = [int(self.pos[0]+self.dir[0]*self.vel*5),int(self.pos[1]+self.dir[1]*self.vel*5)]
	def act(self):
		R = False
		from Main import MainWindow,WIDTH,bullets,level,units,cspots
		p = (int(self.pos[0]),int(self.pos[1])) #point
		t = [] #trail
		for x in range(0,self.vel):
			t.append((int(self.pos[0]-self.dir[0]*x*5),int(self.pos[1]-self.dir[1]*x*5)))
		pygame.draw.line(MainWindow, self.colour,p,t[-1], 1)
		R = bcol(t,level.scol)
		if R:
			for unit in units:
				if Vector.seg_intersect((p,t[-1]),(unit.rect.topright,unit.rect.bottomright)) or Vector.seg_intersect((p,t[-1]),(unit.rect.topleft,unit.rect.bottomleft)):
					if not unit == self.shooter:
						R = True
						unit.hp -= 1
						if unit.hp < 1:
							unit.death()
						break
					else:
						R = False
			if R:
				bullets.remove(self)
		self.dir[0] -= self.dir[0]*self.dra
		self.dir[1] += self.mas
		self.pos[0] += self.dir[0]*self.vel
		self.pos[1] += self.dir[1]*self.vel			
			
class Carbine(object):
	def __init__(self):
		self.ammo = 1
		self.bvel = 20
		self.bdra = 0.001
		self.bma = 0.001
		self.load = 0
	def act(self,user,tar):
		if self.ammo > 0:
			from Main import bullets
			aim = (tar.rect.midtop[0]+random.randint(-15,15),tar.rect.midtop[1]+random.randint(-15,15))
			bullets.append(Bullet(user.rect.center,self.bvel,self.bdra,self.bma,Vector.uvector(user.rect.midtop,aim),user))
			self.ammo-=1
		else:
			self.load +=1
			if self.load == 20:
				self.ammo = 1
				self.load = 0
		
class Soldier(object):
	def __init__(self,t,c,start):
		self.team = t
		self.hp = 10
		self.wdis = 615
		self.weapon = Carbine()
		self.rect = pygame.Rect(0, 0, 10, 18)
		self.start = start
		self.rect.center = start
		self.colour = c
		from Main import level
		self.rect.bottom = level.level[self.rect.centerx][-1].pos[1] + 3
	def act(self):
		from Main import level,HEIGTH, WIDTH, MainWindow, cspots
		prect = self.rect.copy()

		from Main import units
		for unit in units:
			if not unit.team == self.team:
				tar = unit
				break
		if Vector.simple_distance(self.rect.midtop,tar.rect.midtop) < self.wdis:
			sigth = True
			for line in level.ground:
				if Vector.seg_intersect(line,(self.rect.midtop,tar.rect.midtop)):
					sigth = False	
					break					
			if sigth:	#can see target
				if random.randint(-5,5) == 0:
					self.rect.move_ip(1*self.team,0)
					self.rect.bottom = level.level[self.rect.centerx][-1].pos[1] + 3
				self.weapon.act(self,tar)
				#pygame.draw.line(MainWindow, (74,255,34), self.rect.midtop,tar.rect.midtop, 1)
			else:	#can't see a darn thing
				self.rect.move_ip(1*self.team,0)
				self.rect.bottom = level.level[self.rect.centerx][-1].pos[1] + 3
				#pygame.draw.line(MainWindow, (255,74,34), self.rect.midtop,tar.rect.midtop, 1)				
		else:
			self.rect.move_ip(1*self.team,0)
			self.rect.bottom = level.level[self.rect.centerx][-1].pos[1] + 3

		level.scol.blit(level.sstr, prect.topleft, prect) #clears old collision box
		pygame.draw.rect(level.sobj, (255,255,255), prect,0) #clears old sprite
		MainWindow.blit(level.sstr, prect.topleft, prect)    #Draws basic structure on top of the old sprite in Window
		
		
		pygame.draw.rect(level.sobj, self.colour, self.rect,0)
		pygame.draw.rect(level.scol, self.colour, self.rect,0)
		MainWindow.blit(level.sobj,self.rect.topleft,self.rect)
		pygame.display.update((prect,self.rect))

		if self.rect.centerx >= WIDTH-1:
			self.team = -1
			self.colour = (50,200,50)
		if self.rect.centerx <= 0:
			self.team = 1
			self.colour = (200,50,50)
	def death(self):
		from Main import units, MainWindow, level
		level.scol.blit(level.sstr, self.rect.topleft, self.rect) #clears old collision box
		pygame.draw.rect(level.sobj, (255,255,255), self.rect,0) #clears old sprite
		MainWindow.blit(level.sstr, self.rect.topleft, self.rect)    #Draws basic structure on top of the old sprite in Window
		pygame.display.update(self.rect)
		units.remove(self)
		units.append(Soldier(self.team,self.colour,self.start))