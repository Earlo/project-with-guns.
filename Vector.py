import math
from numpy import *

def simple_distance(pos1,pos2):
	return math.sqrt((pos1[0]-pos2[0])**2 + (pos1[1]-pos2[1])**2)
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vector_distance (pos,tar):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	return math.fabs(math.sqrt(vecx**2+vecy**2))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvector(s,e):
	vecx = (e[0] - s[0])
	vecy = (e[1] - s[1])
	length = (math.sqrt(vecx**2+vecy**2))
	unitvector = (vecx/length, vecy/length)
	return unitvector
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvectorl(s,e):
	vecx = (s[0] - e[0])
	vecy = (s[1] - e[1])
	length = (math.sqrt(vecx**2+vecy**2))
	unitvector = (vecx/length, vecy/length,length)
	return unitvector
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def ssift(pos,tar,dist):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	v = (vecx*dist,vecy*dist)
	return v
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sift(pos,tar,len):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	length = math.fabs(math.sqrt(vecx**2+vecy**2))
	unitvector = ((vecx/length)*len, (vecy/length)*len)
	newx = int (round (pos[0] - unitvector[0]))
	newy = int (round (pos[1] - unitvector[1]))
	return (newx,newy)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def ccw(A,B,C):
	return (C[1]-A[1])*(B[0]-A[0]) > (B[1]-A[1])*(C[0]-A[0])
def seg_intersect(A, B):
	return ccw(A[0],B[0],B[1]) != ccw(A[1],B[0],B[1]) and ccw(A[0],A[1],B[0]) != ccw(A[0],A[1],B[1])