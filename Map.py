import pygame
import random
import perlin
import math

import Vector

class block(object):
	def __init__(self,x,y):
			from Main import HEIGTH
			y = HEIGTH - y
			self.pos = (x,y)
			self.support = 1
			self.colour = (150,125,random.randint(125,145))
			global scale
			self.rect = pygame.Rect(x*scale,y*scale,scale,scale)
			
	
class world(object):
	def __init__(self,x,y,sca):
		global scale 
		scale = sca
		from Main import MainWindow
		self.sstr = pygame.Surface((x,y)) #surface for structures (ground, buildings, ect)
		self.sobj = pygame.Surface((x,y))	#surface for objects and dynamic stuff
		self.scol = pygame.Surface((x,y)) #surface for things things can collide to
		self.sstr.fill((255,255,255))
		self.sobj.fill((255,255,255))
		self.scol.fill((255,255,255))
		#self.sstr.set_colorkey((255,255,255))
		self.sobj.set_colorkey((255,255,255))
		#self.scol.set_colorkey((255,255,255))
		self.level = world_gen(x/scale)
		state = 0
		s = 0
		self.hlimits = []
		for y in range(0,len(self.level)-1):
			tile =  self.level[y][-1]
			t = tile.pos[1]
			r = self.level[y+1][-1].pos[1]
			l = self.level[y-1][-1].pos[1]
			if y%15 == 0:
				self.hlimits.append(self.level[y][-5])
		self.hlimits.append(self.level[y][-5])
		if scale == 1:
			for line in self.level:
				for tile in line:
					self.sstr.set_at(tile.pos, tile.colour)
					self.scol.set_at(tile.pos, tile.colour)
		else:
			for line in self.level:
				for tile in line:
					pygame.draw.rect(self.str, tile.colour, tile.rect,0)
		self.ground = []
		for x in range(1,len(self.hlimits)):	
			t = self.hlimits[x].pos
			p = self.hlimits[x-1].pos
			self.ground.append((t,p))
			#pygame.draw.line(self.str, (0,74,34), p,t, 1)

def world_gen(xlen):
	
	Xsca = 0.000008
	Ampsca =  random.uniform(-1.1,1.1)*10
	seed = random.randint(10000,100000)
	print seed
	print Ampsca
	pn = perlin.SimplexNoise(seed)
	map = []
	for x in range(0,xlen):
		map.append([])
		Amp = pn.noise2(x*Xsca,x*Xsca)*150
		for y in range(0,100 + int((pn.noise2(x*Xsca,x*Xsca)*Amp+pn.noise2(x*Xsca,x*Xsca)*Amp/5+pn.noise2(x*Xsca,x*Xsca)*Amp/50))):
			map[x].append(block(x,y))	
	return map